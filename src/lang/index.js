import en from './en'
import it from './it'
import fr from './fr'

export default {
	en,
	it,
	fr
}
