export default {
	slogan: '<strong>Digiflashcards</strong> permette di creare <strong>flashcards</strong> multimediali e interattive.',
	soutien: 'Voglio supportare questo progetto ❤️.',
	codeSource: 'Codice sorgente',
	langueModifiee: 'Lingua modificata.',
	creerSerie: 'Crea un set di flashcards',
	nouvelleSerie: 'Nuovo set di flashcards',
	nomSerie: 'Nome del set di flashcards',
	questionSecrete: 'Domanda segreta',
	reponseSecrete: 'Risposta segreta',
	reponseSecreteEdition: 'Risposta segreta (per modifiche successive)',
	creer: 'Crea',
	erreurServeur: 'Errore di comunicazione con il server.',
	remplirTitre: 'Completa il campo "Nome del set di flashcards".',
	selectionnerQuestionSecrete: 'Seleziona la domanda segreta.',
	remplirReponseSecrete: 'Completa il campo "Risposta segreta".',
	motPrefere: 'Qual è la mia parola preferita?',
	filmPrefere: 'Qual è il mio film preferito?',
	chansonPreferee: 'Qual è la mia canzone preferita?',
	prenomMere: 'Qual è il nome di mia madre?',
	prenomPere: 'Qual è il nome di mio padre?',
	nomRue: 'Qual è il nome della mia via?',
	nomEmployeur: 'Qual è il nome del mio capo?',
	nomAnimal: 'Qual è il nome del mio animale domestico?',
	accueil: 'Home',
	supprimer: 'Elimina',
	aucuneCarte: 'Non ci sono flashcards in questo momento.',
	credits: 'Set di flashcards create con <span>Digiflashcards by La Digitale</span>',
	lienEtCodeQR: 'Link e QR code:',
	copierLien: 'Copia il link',
	afficherCodeQR: 'Visualizza il QR code',
	codeIntegration: 'Codice per incorporamento:',
	copierCode: 'Copia il codice per l\'incorporamento',
	ajouter: 'Aggiungi',
	modifier: 'Modifica',
	debloquerSerie: 'Sblocca questo set',
	valider: 'Conferma',
	parametresSerie: 'Opzioni del set',
	modifierNomSerie: 'Cambia il nome del set',
	modifierAccesSerie: 'Cambia l\'accesso al set',
	supprimerSerie: 'Elimina il set',
	terminerSession: 'Termina la modifica',
	nouveauNom: 'Nuovo nome',
	questionSecreteActuelle: 'Domanda segreta attuale',
	reponseSecreteActuelle: 'Risposta segreta attuale',
	nouvelleQuestionSecrete: 'Nuova domanda segreta',
	nouvelleReponseSecrete: 'Nuova risposta segreta',
	confirmationSupprimerSerie: 'Vuoi eliminare il set e tutti i suoi contenuti?',
	confirmationSupprimerCarte: 'Vuoi eliminare questa flashcard?',
	actionNonAutorisee: 'Non sei autorizzato ad effettuare questa azione.',
	selectionnerQuestionSecreteActuelle: 'Scegli la domanda segreta attuale.',
	indiquerReponseSecreteActuelle: 'Indica la risposta segreta attuale.',
	selectionnerNouvelleQuestionSecrete: 'Seleziona la nuova domanda segreta.',
	indiquerNouvelleReponseSecrete: 'Inserisci la nuova risposta segreta.',
	non: 'No',
	oui: 'Si',
	codeQR: 'QR code',
	lienCopie: 'Link copiato negli appunti.',
	codeCopie: 'Codice di incorporamento copiato negli appunti.',
	nomModifie: 'Nome modificato.',
	accesModifie: 'Accesso modificato.',
	sessionTerminee: 'Sessione terminata.',
	remplirChampNouveauNom: 'Compila il campo "Nuovo nome".',
	informationsIncorrectes: 'La domanda e la risposta segreta non sono corrette.',
	serieDebloquee: 'Set sbloccato.',
	exporterSerie: 'Esporta il set',
	importerSerie: 'Importa il set',
	serieImportee: 'Set importato.',
	serieExportee: 'Set esportato.',
	selectionnerFichier: 'Selezione il file .zip',
	alerteImporter: 'Tutti i contenuti del set saranno sostituiti dai contenuti importati.',
	ajouterImage: 'Aggiungi un\'immagine',
	terme: 'Termini',
	definition: 'Definizione',
	ajouterCarte: 'Aggiungi una carta',
	carteSupprimee: 'Flashcard eliminata.',
	afficherImage: 'Visualizza immagine',
	erreurTeleversement: 'Errore nel caricamento del file.',
	tailleMax: 'La dimensione massima permessa è {taille} Mb.',
	apercuApprenant: 'Vista dello studente',
	fermer: 'Chiudi',
	ecrivezTerme: 'Scrivi la parola corrispondente.',
	ajouterAudio: 'Aggiungi audio',
	afficherAudio: 'Visualizza audio',
	enregistrerAudio: 'Registrare audio',
	selectionnerFichierMP3: 'Selezionare un file .mp3',
	ou: 'o',
	annuler: 'Annullare',
	enregistrementAudio: 'Registrazione audio',
	erreurMicro: 'Il microfono non dispone delle autorizzazioni necessarie per avviare la registrazione.',
	aucuneEntreeAudio: 'Non è stato rilevato alcun ingresso audio sul dispositivo.',
	erreurFormat: 'Il formato di questo file non è valido.',
	importerCSV: 'Importazione .csv',
	telechargerModele: 'Scaricare il modello',
	cartesImportees: 'Flashcards importate.',
	cartesMelangees: 'Flashcards mescolate.',
	cartesInversees: 'Flashcards invertite.',
	transcodage: 'Transcodifica in mp3, attendere prego...',
	enregistrementNonSupporte: 'Il browser non è supportato.',
	afficherCartePrecedente: 'Mostra la flashcard precedente',
	afficherCarteSuivante: 'Mostra la seguente flashcard',
	afficherQuestionPrecedente: 'Mostra la domanda precedente',
	afficherQuestionSuivante: 'Mostra la seguente domanda',
	melangerCartes: 'Mescolare le flashcards',
	inverserCartes: 'Invertire le flashcards',
	mettrePleinEcran: 'Imposta a schermo intero',
	sortirPleinEcran: 'Uscire a schermo intero',
	reinitialiser: 'Reset',
	afficherScore: 'Mostra il punteggio',
	afficherCartes: 'Mostra Flashcards',
	afficherQuiz: 'Mostra Quiz',
	afficherEcrire: 'Mostra Scrivere',
	optionExercices: 'Attivare gli esercizi',
	optionCasse: 'Siate sensibili alle maiuscole e minuscole nelle risposte di <i>Scrivere</i>.'
}
